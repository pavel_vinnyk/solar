window.onload=function(){

    var planetRadius = 50,   // радиус окружности 
        planetDelay = 10;  // задержка (в мсек.) 

    var moonRadius = 20,
        moonDelay = 15,
        moonAngle = 0;

    var da = 1*Math.PI/180;   // приращение угла 
    
    var solarSystem = [
      document.getElementById("mercury"),
      document.getElementById("venus"),
      document.getElementById("earth"),
      document.getElementById("mars"),
      document.getElementById("jupiter"),
      document.getElementById("saturn"),
      document.getElementById("uranus"),
      document.getElementById("neptune"),
      document.getElementById("plato")
    ];

    for (var i=0;i<9;++i){

      solarSystem[i].radius = planetRadius;
      solarSystem[i].angle = 0;
      solarSystem[i].delay = planetDelay;
      planetRadius += 25; 
      planetDelay += 5;  
    }
        
    setInterval (
        function () {
          moon.style.left =8+moonRadius*Math.cos(moonAngle)+"px";
          moon.style.top  =8+moonRadius*Math.sin(moonAngle)+"px";    
          moonAngle+=da;
        }, moonDelay);

    function planetRotate (planet) {
          planet.style.left =planet.radius+planet.radius*Math.cos(planet.angle)+"px";
          planet.style.top  =planet.radius+planet.radius*Math.sin(planet.angle)+"px";    
          planet.angle+=da;
    };
    var len=solarSystem.length;
    for (var i=0;i<len;++i){
        setInterval (planetRotate,solarSystem[i].delay,solarSystem[i]);
    }
    
    
};
